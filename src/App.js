import './App.css';
import OrderSum from "./components/OrderSum/OrderSum";

function App() {
  return (
    <div className="App">
       <OrderSum/>
    </div>
  );
}

export default App;
