import React from 'react';

const EqualCheck = ({equalState, setEqualState}) => {
   const onChangeInput = (event, type) => {
      console.log(event.target.value);
      if ((isNaN(parseInt(event.target.value)) && event.target.value !== '')){
         return;
      }
      setEqualState((prev) => {
         return {...prev, [type]:event.target.value === '' ? 0 : parseInt(event.target.value)};
      });
   }
   return (
     <div className="currentOption">
        <label>
           Человек:
           <input
             required
             placeholder="кол-во"
             type="number"
             value={equalState.people === 0 ? '' : equalState.people}
             onChange={(event) => onChangeInput(event, 'people')}
           />
        </label>
        <label>
           Cумма заказа:
           <input
             required
             placeholder="сом"
             type="number"
             value={equalState.totalPrice === 0 ? '' : equalState.totalPrice}
             onChange={(event) => onChangeInput(event, 'totalPrice')}
           />
        </label>
        <label>
           Процент чаевых:
           <input
             placeholder="%"
             type="number"
             value={equalState.tipsPercentage === 0 ? '' : equalState.tipsPercentage}
             onChange={(event) => onChangeInput(event, 'tipsPercentage')}
           />
        </label>
        <label>
           Доставка:
           <input
             placeholder="сом"
             type="number"
             value={equalState.delivery === 0 ? '' : equalState.delivery}
             onChange={(event) => onChangeInput(event, 'delivery')}
           />
        </label>
     </div>
   );
};

export default EqualCheck;