import React, {useState} from 'react';
import './OrderSum.css';
import {nanoid} from "nanoid";
import Options from "./Options/Options";
import IndividualCheck from "./IndividualCheck/IndividualCheck";
import EqualCheck from "./EqualCheck/EqualCheck";
import SubmittedMessage from "./SubmittedMessage/SubmittedMessage";

class SomeMan {
   constructor() {
      this.name = 'Человек ' + this._plusCount();
      this.price = Math.floor(Math.random() * 500);
      this.id = nanoid();
   }

   static count = 0;

   _plusCount() {
      return ++SomeMan.count;
   }
}

const initEqualState = {
   totalPrice: 1000,
   people: 1,
   tipsPercentage: 10,
   delivery: 100,
};
const initIndivState = {
   people: [new SomeMan()],
   tipsPercentage: 15,
   delivery: 50,
};

const OrderSum = () => {
   const [selectedOption, setSelectedOption] = useState('individualCheck');
   const [submitted, setSubmitted] = useState(false);
   const [indivState, setIndivState] = useState(initIndivState);
   const [equalState, setEqualState] = useState(initEqualState);

   const onChangeOption = (event) => {
      setSelectedOption(event.target.value);
      if (submitted) {
         setSubmitted(false);
      }
   };

   const changePersonName = (event, id) => {
      setIndivState((prev) => {
         return {
            ...prev, people: prev.people.map((man) => {
               if (id === man.id) {
                  man.name = event.target.value;
               }
               return man;
            })
         }
      })
   };

   const changePersonPrice = (event, id) => {
      setIndivState((prev) => {
         return {
            ...prev, people: prev.people.map((man) => {
               const number = parseInt(event.target.value);
               if (id === man.id && !isNaN(number)) {
                  if (number > 0) {
                     man.price = number;
                  } else {
                     man.price = 0;
                  }
               } else if (event.target.value === '' && id === man.id) {
                  man.price = 0;
               }
               return man;
            })
         }
      })
   };

   const removePerson = (id) => {
      setIndivState((prev) => {
         const newState = {...prev};
         newState.people.forEach((man) => {
            if (id === man.id) {
               newState.people.splice(newState.people.indexOf(man), 1);
            }
         })
         return newState;
      })
   };

   const changeTips = (event) => {
      setIndivState((prev) => {
         return {...prev, tipsPercentage: event.target.value === '' ? 0 : parseInt(event.target.value)}
      });
   };

   const changeDeliveryPrice = (event) => {
      setIndivState((prev) => {
         return {...prev, delivery: event.target.value === '' ? 0 : parseInt(event.target.value)};
      });
   };

   const onSubmit = (event) => {
      event.preventDefault();
      setSubmitted((prev) => {
         return !prev;
      });
   };

   const addPerson = () => {
      setIndivState((prev) => {
         const newState = {...prev};
         newState.people.push(new SomeMan());
         return newState;
      })
   };

   const isOption = {
      equalCheck: (
        <EqualCheck
          equalState={equalState}
          setEqualState={setEqualState}
        />
      ),
      individualCheck: (
        <IndividualCheck
          indivState={indivState}
          changePersonName={changePersonName}
          changePersonPrice={changePersonPrice}
          removePerson={removePerson}
          addPerson={addPerson}
          changeTips={changeTips}
          changeDeliveryPrice={changeDeliveryPrice}
        />
      ),
   };

   return (
     <form onSubmit={onSubmit}>
        <Options
          selectedOption={selectedOption}
          onChangeOption={onChangeOption}
        />
        {isOption[selectedOption]}
        <button type="submit" className="resultBtn">Расчитать</button>
        {submitted ?
          <SubmittedMessage selectedOption={selectedOption} indivState={indivState} equalState={equalState}/> : null}
     </form>
   );
};

export default OrderSum;