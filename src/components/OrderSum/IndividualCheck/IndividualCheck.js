import React from 'react';
import Person from "./Person/Person";

const IndividualCheck = (props) => {
   return (
     <div className="currentOption">
        {props.indivState.people.map((man) => {
           return (
             <Person
               price={man.price}
               name={man.name}
               key={man.id}
               changePersonName={(event) => props.changePersonName(event, man.id)}
               changePersonPrice={(event) => props.changePersonPrice(event, man.id)}
               removePerson={() => props.removePerson(man.id)}
             />)
        })}
        <div>
           <button className="addBtn" type="button" onClick={props.addPerson}>Add</button>
        </div>
        <label>
           Процент чаевых:{' '}
           <input
             type="number"
             value={props.indivState.tipsPercentage === 0 ? '' : props.indivState.tipsPercentage}
             onChange={props.changeTips}
           />
        </label>
        <label>
           Доставка:{' '}
           <input
             type="number"
             value={props.indivState.delivery === 0 ? '' : props.indivState.delivery}
             onChange={props.changeDeliveryPrice}
           />
        </label>
     </div>
   );
};

export default IndividualCheck;