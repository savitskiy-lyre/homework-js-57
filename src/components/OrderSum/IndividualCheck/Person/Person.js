import React from 'react';

const Person = (props) => {
   return (
     <div>
        <input
          type="text"
          value={props.name}
          onChange={props.changePersonName}
          placeholder="Имя"
          required
        />
        <input
          type="number"
          value={props.price === 0 ? '' : props.price}
          onChange={props.changePersonPrice}
          placeholder="Сумма"
          required
        />
        <span>сом</span>
        <button type="button" onClick={props.removePerson}>Remove</button>
     </div>
   );
};

export default Person;