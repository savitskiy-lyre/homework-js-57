import React from 'react';
import './Options.css';

const Options = ({selectedOption, onChangeOption}) => {
   return (
     <div className='options'>
        <p>Сумма заказа считается:</p>
        <label htmlFor="equalCheck">
           <input
             type="radio"
             value='equalCheck'
             id='equalCheck'
             name='priceType'
             checked={selectedOption === "equalCheck"}
             onChange={onChangeOption}
           />
           Поровну между всеми участниками
        </label>
        <label htmlFor="individualCheck">
           <input
             type="radio"
             value='individualCheck'
             id='individualCheck'
             name='priceType'
             checked={selectedOption === 'individualCheck'}
             onChange={onChangeOption}
           />
           Каждому индивидуально
        </label>
     </div>
   );
};

export default Options;