import React from 'react';

const SubmittedMessage = ({selectedOption, indivState, equalState}) => {
   const getTotalPrice = () => {
      let totalPrice;
      if (selectedOption === 'individualCheck'){
         totalPrice = indivState.people.reduce((total, man) => total + man.price, 0);
         totalPrice += totalPrice * (indivState.tipsPercentage) / 100;
         totalPrice += indivState.delivery;
      } else if (selectedOption === 'equalCheck') {
         totalPrice = equalState.totalPrice + equalState.totalPrice * equalState.tipsPercentage / 100;
         totalPrice += equalState.delivery;
      }
      return totalPrice;
   };

   if (selectedOption === 'equalCheck') {
      const total = getTotalPrice();
      return (
        <div>
           <p>Общая сумма: <strong>{Math.round(total)}</strong> сом</p>
           <p>Количество человек: <strong>{equalState.people}</strong></p>
           <p>Каждый платит по: <strong>{Math.round(total / equalState.people)}</strong> сом</p>
        </div>
      );
   } else if (selectedOption === 'individualCheck') {
      return (
        <div>
           <p>
              {'Общая сумма: '}
              <strong>{Math.round(getTotalPrice())}</strong>
              {' сом'}
           </p>
           {indivState.people.map((man) => {
              return (
                <p key={man.id + man.name}>
                   {man.name + ': '}
                   <strong>{Math.round(man.price + (man.price * indivState.tipsPercentage / 100) + indivState.delivery / indivState.people.length)}</strong>
                   {' сом'}
                </p>
              );
           })}
        </div>
      );
   }
};

export default SubmittedMessage;